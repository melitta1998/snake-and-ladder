﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    internal class Board: Player
    {
        int A0;
        int A1;

        public void set()
        {
            A0 = 1;
            A1 = 1;
        }
        public void Calc()
        {
            if (turn == 0 && (A0 + number) <= 100)
            {
                A0 = A0 + number;
                if (A0 == 7)
                {

                    A0 = 14;
                }
                if (A0 == 2)
                {

                    A0 = 23;
                }
                if (A0 == 15)
                {

                    A0 = 26;
                }
                if (A0 == 8)
                {

                    A0 = 31;
                }
                if (A0 == 21)
                {

                    A0 = 42;
                }
                if (A0 == 28)
                {

                    A0 = 84;
                }
                if (A0 == 51)
                {

                    A0 = 67;
                }
                if (A0 == 71)
                {

                    A0 = 91;
                }
                if (A0 == 87)
                {

                    A0 = 94;
                }
                if (A0 == 78)
                {

                    A0 = 98;
                }
                if (A0 == 16)
                {

                    A0 = 6;
                }
                if (A0 == 49)
                {

                    A0 = 11;
                }
                if (A0 == 46)
                {

                    A0 = 25;
                }
                if (A0 == 62)
                {

                    A0 = 19;
                }
                if (A0 == 64)
                {

                    A0 = 60;
                }
                if (A0 == 74)
                {

                    A0 = 53;
                }
                if (A0 == 89)
                {

                    A0 = 68;
                }
                if (A0 == 92)
                {

                    A0 = 88;
                }
                if (A0 == 95)
                {

                    A0 = 75;
                }
                if (A0 == 99)
                {

                    A0 = 80;
                }
            }
            if (turn == 1 && (A1 + number) <= 100)
            {
                A1 = A1 + number;
                if (A1 == 7)
                {

                    A1 = 14;
                }
                if (A1 == 2)
                {

                    A1 = 23;
                }
                if (A1 == 15)
                {

                    A1 = 26;
                }
                if (A1 == 8)
                {

                    A1 = 31;
                }
                if (A1 == 21)
                {

                    A1 = 42;
                }
                if (A1 == 28)
                {

                    A1 = 84;
                }
                if (A1 == 51)
                {

                    A1 = 67;
                }
                if (A1 == 71)
                {

                    A1 = 91;
                }
                if (A1 == 87)
                {

                    A1 = 94;
                }
                if (A1 == 78)
                {

                    A1 = 98;
                }
                if (A1 == 16)
                {

                    A1 = 6;
                }
                if (A1 == 49)
                {

                    A1 = 11;
                }
                if (A1 == 46)
                {

                    A1 = 25;
                }
                if (A1 == 62)
                {

                    A1 = 19;
                }
                if (A1 == 64)
                {

                    A1 = 60;
                }
                if (A1 == 74)
                {

                    A1 = 53;
                }
                if (A1 == 89)
                {

                    A1 = 68;
                }
                if (A1 == 92)
                {

                    A1 = 88;
                }
                if (A1 == 95)
                {

                    A1 = 75;
                }
                if (A1 == 99)
                {

                    A1 = 80;
                }
            }
        }

        public int MoveA()
        {

            return A0;
        }

        public int MoveB()
        {
            return A1;
        }

    }
}
