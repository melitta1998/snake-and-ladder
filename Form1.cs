﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        Board b = new Board();
        void reinitializare()
        {

            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;
            pictureBox15.Visible = false;
            pictureBox16.Visible = false;
            pictureBox17.Visible = false;
            pictureBox18.Visible = false;
            pictureBox19.Visible = false;
            pictureBox20.Visible = false;
            pictureBox21.Visible = false;
            pictureBox22.Visible = false;
            pictureBox23.Visible = false;
            pictureBox24.Visible = false;
            pictureBox25.Visible = false;
            pictureBox26.Visible = false;
            pictureBox27.Visible = false;
            pictureBox28.Visible = false;
            pictureBox29.Visible = false;
            pictureBox30.Visible = false;
            pictureBox31.Visible = false;
            pictureBox32.Visible = false;
            pictureBox33.Visible = false;
            pictureBox34.Visible = false;
            pictureBox35.Visible = false;
            pictureBox36.Visible = false;
            pictureBox37.Visible = false;
            pictureBox38.Visible = false;
            pictureBox39.Visible = false;
            pictureBox40.Visible = false;
            pictureBox41.Visible = false;
            pictureBox42.Visible = false;
            pictureBox43.Visible = false;
            pictureBox44.Visible = false;
            pictureBox45.Visible = false;
            pictureBox46.Visible = false;
            pictureBox47.Visible = false;
            pictureBox48.Visible = false;
            pictureBox49.Visible = false;
            pictureBox50.Visible = false;
            pictureBox51.Visible = false;
            pictureBox52.Visible = false;
            pictureBox53.Visible = false;
            pictureBox54.Visible = false;
            pictureBox55.Visible = false;
            pictureBox56.Visible = false;
            pictureBox57.Visible = false;
            pictureBox58.Visible = false;
            pictureBox59.Visible = false;
            pictureBox60.Visible = false;
            pictureBox61.Visible = false;
            pictureBox62.Visible = false;
            pictureBox63.Visible = false;
            pictureBox64.Visible = false;
            pictureBox65.Visible = false;
            pictureBox66.Visible = false;
            pictureBox67.Visible = false;
            pictureBox68.Visible = false;
            pictureBox69.Visible = false;
            pictureBox70.Visible = false;
            pictureBox71.Visible = false;
            pictureBox72.Visible = false;
            pictureBox73.Visible = false;
            pictureBox74.Visible = false;
            pictureBox75.Visible = false;
            pictureBox76.Visible = false;
            pictureBox77.Visible = false;
            pictureBox78.Visible = false;
            pictureBox79.Visible = false;
            pictureBox80.Visible = false;
            pictureBox81.Visible = false;
            pictureBox82.Visible = false;
            pictureBox83.Visible = false;
            pictureBox84.Visible = false;
            pictureBox85.Visible = false;
            pictureBox86.Visible = false;
            pictureBox87.Visible = false;
            pictureBox88.Visible = false;
            pictureBox89.Visible = false;
            pictureBox90.Visible = false;
            pictureBox91.Visible = false;
            pictureBox92.Visible = false;
            pictureBox93.Visible = false;
            pictureBox94.Visible = false;
            pictureBox95.Visible = false;
            pictureBox96.Visible = false;
            pictureBox97.Visible = false;
            pictureBox98.Visible = false;
            pictureBox99.Visible = false;
            pictureBox100.Visible = false;
        }
        private void buttonStart_Click(object sender, EventArgs e)
        {
            buttonZar.Visible = true;
            b.set();
            buttonStart.Visible = false;

        }
        private void buttonZar_Click(object sender, EventArgs e)
        {
           
            labelValZar.Text = b.Roll().ToString();
            if(b.getTurn()==0)
            {
                pictureBoxTurn.ImageLocation = @"cat.png";
            }
            else if (b.getTurn() == 1)
            {
                pictureBoxTurn.ImageLocation = @"dog.png";
            }

            b.Calc();

            reinitializare();

            switch (b.MoveA())
            {
                case 1:
                    {
                        pictureBox1.Visible = true;
                        pictureBox1.ImageLocation = @"cat.png";
                        break;
                    }
                case 2:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.ImageLocation = @"cat.png";
                        break;
                    }
                case 3:
                    {
                        pictureBox3.Visible = true;
                        pictureBox3.ImageLocation = @"cat.png";
                        break;
                    }
                case 4:
                    {
                        pictureBox4.Visible = true;
                        pictureBox4.ImageLocation = @"cat.png";
                        break;
                    }
                case 5:
                    {
                        pictureBox5.Visible = true;
                        pictureBox5.ImageLocation = @"cat.png";
                        break;
                    }
                case 6:
                    {
                        pictureBox6.Visible = true;
                        pictureBox6.ImageLocation = @"cat.png";
                        break;
                    }
                case 7:
                    {
                        pictureBox7.Visible = true;
                        pictureBox7.ImageLocation = @"cat.png";
                        break;
                    }
                case 8:
                    {
                        pictureBox8.Visible = true;
                        pictureBox8.ImageLocation = @"cat.png";
                        break;
                    }
                case 9:
                    {
                        pictureBox9.Visible = true;
                        pictureBox9.ImageLocation = @"cat.png";
                        break;
                    }
                case 10:
                    {
                        pictureBox10.Visible = true;
                        pictureBox10.ImageLocation = @"cat.png";
                        break;
                    }
                case 11:
                    {
                        pictureBox11.Visible = true;
                        pictureBox11.ImageLocation = @"cat.png";
                        break;
                    }
                case 12:
                    {
                        pictureBox12.Visible = true;
                        pictureBox12.ImageLocation = @"cat.png";
                        break;
                    }
                case 13:
                    {
                        pictureBox13.Visible = true;
                        pictureBox13.ImageLocation = @"cat.png";
                        break;
                    }
                case 14:
                    {
                        pictureBox14.Visible = true;
                        pictureBox14.ImageLocation = @"cat.png";
                        break;
                    }
                case 15:
                    {
                        pictureBox15.Visible = true;
                        pictureBox15.ImageLocation = @"cat.png";
                        break;
                    }
                case 16:
                    {
                        pictureBox16.Visible = true;
                        pictureBox16.ImageLocation = @"cat.png";
                        break;
                    }
                case 17:
                    {
                        pictureBox17.Visible = true;
                        pictureBox17.ImageLocation = @"cat.png";
                        break;
                    }
                case 18:
                    {
                        pictureBox18.Visible = true;
                        pictureBox18.ImageLocation = @"cat.png";
                        break;
                    }
                case 19:
                    {
                        pictureBox19.Visible = true;
                        pictureBox19.ImageLocation = @"cat.png";
                        break;
                    }
                case 20:
                    {
                        pictureBox20.Visible = true;
                        pictureBox20.ImageLocation = @"cat.png";
                        break;
                    }
                case 21:
                    {
                        pictureBox21.Visible = true;
                        pictureBox21.ImageLocation = @"cat.png";
                        break;
                    }
                case 22:
                    {
                        pictureBox22.Visible = true;
                        pictureBox22.ImageLocation = @"cat.png";
                        break;
                    }
                case 23:
                    {
                        pictureBox23.Visible = true;
                        pictureBox23.ImageLocation = @"cat.png";
                        break;
                    }
                case 24:
                    {
                        pictureBox24.Visible = true;
                        pictureBox24.ImageLocation = @"cat.png";
                        break;
                    }
                case 25:
                    {
                        pictureBox25.Visible = true;
                        pictureBox25.ImageLocation = @"cat.png";
                        break;
                    }
                case 26:
                    {
                        pictureBox26.Visible = true;
                        pictureBox26.ImageLocation = @"cat.png";
                        break;
                    }
                case 27:
                    {
                        pictureBox27.Visible = true;
                        pictureBox27.ImageLocation = @"cat.png";
                        break;
                    }
                case 28:
                    {
                        pictureBox28.Visible = true;
                        pictureBox28.ImageLocation = @"cat.png";
                        break;
                    }
                case 29:
                    {
                        pictureBox29.Visible = true;
                        pictureBox29.ImageLocation = @"cat.png";
                        break;
                    }
                case 30:
                    {
                        pictureBox30.Visible = true;
                        pictureBox30.ImageLocation = @"cat.png";
                        break;
                    }
                case 31:
                    {
                        pictureBox31.Visible = true;
                        pictureBox31.ImageLocation = @"cat.png";
                        break;
                    }
                case 32:
                    {
                        pictureBox32.Visible = true;
                        pictureBox32.ImageLocation = @"cat.png";
                        break;
                    }
                case 33:
                    {
                        pictureBox33.Visible = true;
                        pictureBox33.ImageLocation = @"cat.png";
                        break;
                    }
                case 34:
                    {
                        pictureBox34.Visible = true;
                        pictureBox34.ImageLocation = @"cat.png";
                        break;
                    }
                case 35:
                    {
                        pictureBox35.Visible = true;
                        pictureBox35.ImageLocation = @"cat.png";
                        break;
                    }
                case 36:
                    {
                        pictureBox36.Visible = true;
                        pictureBox36.ImageLocation = @"cat.png";
                        break;
                    }
                case 37:
                    {
                        pictureBox37.Visible = true;
                        pictureBox37.ImageLocation = @"cat.png";
                        break;
                    }
                case 38:
                    {
                        pictureBox38.Visible = true;
                        pictureBox38.ImageLocation = @"cat.png";
                        break;
                    }
                case 39:
                    {
                        pictureBox39.Visible = true;
                        pictureBox39.ImageLocation = @"cat.png";
                        break;
                    }
                case 40:
                    {
                        pictureBox40.Visible = true;
                        pictureBox40.ImageLocation = @"cat.png";
                        break;
                    }
                case 41:
                    {
                        pictureBox41.Visible = true;
                        pictureBox41.ImageLocation = @"cat.png";
                        break;
                    }
                case 42:
                    {
                        pictureBox42.Visible = true;
                        pictureBox42.ImageLocation = @"cat.png";
                        break;
                    }
                case 43:
                    {
                        pictureBox43.Visible = true;
                        pictureBox43.ImageLocation = @"cat.png";
                        break;
                    }
                case 44:
                    {
                        pictureBox44.Visible = true;
                        pictureBox44.ImageLocation = @"cat.png";
                        break;
                    }
                case 45:
                    {
                        pictureBox45.Visible = true;
                        pictureBox45.ImageLocation = @"cat.png";
                        break;
                    }
                case 46:
                    {
                        pictureBox46.Visible = true;
                        pictureBox46.ImageLocation = @"cat.png";
                        break;
                    }
                case 47:
                    {
                        pictureBox47.Visible = true;
                        pictureBox48.ImageLocation = @"cat.png";
                        break;
                    }
                case 48:
                    {
                        pictureBox48.Visible = true;
                        pictureBox48.ImageLocation = @"cat.png";
                        break;
                    }
                case 49:
                    {
                        pictureBox49.Visible = true;
                        pictureBox49.ImageLocation = @"cat.png";
                        break;
                    }
                case 50:
                    {
                        pictureBox50.Visible = true;
                        pictureBox50.ImageLocation = @"cat.png";
                        break;
                    }
                case 51:
                    {
                        pictureBox51.Visible = true;
                        pictureBox51.ImageLocation = @"cat.png";
                        break;
                    }
                case 52:
                    {
                        pictureBox52.Visible = true;
                        pictureBox52.ImageLocation = @"cat.png";
                        break;
                    }
                case 53:
                    {
                        pictureBox53.Visible = true;
                        pictureBox53.ImageLocation = @"cat.png";
                        break;
                    }
                case 54:
                    {
                        pictureBox54.Visible = true;
                        pictureBox54.ImageLocation = @"cat.png";
                        break;
                    }
                case 55:
                    {
                        pictureBox55.Visible = true;
                        pictureBox55.ImageLocation = @"cat.png";
                        break;
                    }
                case 56:
                    {
                        pictureBox56.Visible = true;
                        pictureBox56.ImageLocation = @"cat.png";
                        break;
                    }
                case 57:
                    {
                        pictureBox57.Visible = true;
                        pictureBox57.ImageLocation = @"cat.png";
                        break;
                    }
                case 58:
                    {
                        pictureBox58.Visible = true;
                        pictureBox58.ImageLocation = @"cat.png";
                        break;
                    }
                case 59:
                    {
                        pictureBox59.Visible = true;
                        pictureBox59.ImageLocation = @"cat.png";
                        break;
                    }
                case 60:
                    {
                        pictureBox60.Visible = true;
                        pictureBox60.ImageLocation = @"cat.png";
                        break;
                    }
                case 61:
                    {
                        pictureBox61.Visible = true;
                        pictureBox61.ImageLocation = @"cat.png";
                        break;
                    }
                case 62:
                    {
                        pictureBox62.Visible = true;
                        pictureBox62.ImageLocation = @"cat.png";
                        break;
                    }
                case 63:
                    {
                        pictureBox63.Visible = true;
                        pictureBox63.ImageLocation = @"cat.png";
                        break;
                    }
                case 64:
                    {
                        pictureBox64.Visible = true;
                        pictureBox64.ImageLocation = @"cat.png";
                        break;
                    }
                case 65:
                    {
                        pictureBox65.Visible = true;
                        pictureBox65.ImageLocation = @"cat.png";
                        break;
                    }
                case 66:
                    {
                        pictureBox66.Visible = true;
                        pictureBox66.ImageLocation = @"cat.png";
                        break;
                    }
                case 67:
                    {
                        pictureBox67.Visible = true;
                        pictureBox67.ImageLocation = @"cat.png";
                        break;
                    }
                case 68:
                    {
                        pictureBox68.Visible = true;
                        pictureBox68.ImageLocation = @"cat.png";
                        break;
                    }
                case 69:
                    {
                        pictureBox69.Visible = true;
                        pictureBox69.ImageLocation = @"cat.png";
                        break;
                    }
                case 70:
                    {
                        pictureBox70.Visible = true;
                        pictureBox70.ImageLocation = @"cat.png";
                        break;
                    }
                case 71:
                    {
                        pictureBox71.Visible = true;
                        pictureBox71.ImageLocation = @"cat.png";
                        break;
                    }
                case 72:
                    {
                        pictureBox72.Visible = true;
                        pictureBox72.ImageLocation = @"cat.png";
                        break;
                    }
                case 73:
                    {
                        pictureBox73.Visible = true;
                        pictureBox73.ImageLocation = @"cat.png";
                        break;
                    }
                case 74:
                    {
                        pictureBox74.Visible = true;
                        pictureBox74.ImageLocation = @"cat.png";
                        break;
                    }
                case 75:
                    {
                        pictureBox75.Visible = true;
                        pictureBox75.ImageLocation = @"cat.png";
                        break;
                    }
                case 76:
                    {
                        pictureBox76.Visible = true;
                        pictureBox76.ImageLocation = @"cat.png";
                        break;
                    }
                case 77:
                    {
                        pictureBox77.Visible = true;
                        pictureBox77.ImageLocation = @"cat.png";
                        break;
                    }
                case 78:
                    {
                        pictureBox78.Visible = true;
                        pictureBox78.ImageLocation = @"cat.png";
                        break;
                    }
                case 79:
                    {
                        pictureBox79.Visible = true;
                        pictureBox79.ImageLocation = @"cat.png";
                        break;
                    }
                case 80:
                    {
                        pictureBox80.Visible = true;
                        pictureBox80.ImageLocation = @"cat.png";
                        break;
                    }
                case 81:
                    {
                        pictureBox81.Visible = true;
                        pictureBox81.ImageLocation = @"cat.png";
                        break;
                    }
                case 82:
                    {
                        pictureBox82.Visible = true;
                        pictureBox82.ImageLocation = @"cat.png";
                        break;
                    }
                case 83:
                    {
                        pictureBox83.Visible = true;
                        pictureBox83.ImageLocation = @"cat.png";
                        break;
                    }
                case 84:
                    {
                        pictureBox84.Visible = true;
                        pictureBox84.ImageLocation = @"cat.png";
                        break;
                    }
                case 85:
                    {
                        pictureBox85.Visible = true;
                        pictureBox85.ImageLocation = @"cat.png";
                        break;
                    }
                case 86:
                    {
                        pictureBox86.Visible = true;
                        pictureBox86.ImageLocation = @"cat.png";
                        break;
                    }
                case 87:
                    {
                        pictureBox87.Visible = true;
                        pictureBox87.ImageLocation = @"cat.png";
                        break;
                    }
                case 88:
                    {
                        pictureBox88.Visible = true;
                        pictureBox88.ImageLocation = @"cat.png";
                        break;
                    }
                case 89:
                    {
                        pictureBox89.Visible = true;
                        pictureBox89.ImageLocation = @"cat.png";
                        break;
                    }
                case 90:
                    {
                        pictureBox90.Visible = true;
                        pictureBox90.ImageLocation = @"cat.png";
                        break;
                    }
                case 91:
                    {
                        pictureBox91.Visible = true;
                        pictureBox91.ImageLocation = @"cat.png";
                        break;
                    }
                case 92:
                    {
                        pictureBox92.Visible = true;
                        pictureBox92.ImageLocation = @"cat.png";
                        break;
                    }
                case 93:
                    {
                        pictureBox93.Visible = true;
                        pictureBox93.ImageLocation = @"cat.png";
                        break;
                    }
                case 94:
                    {
                        pictureBox94.Visible = true;
                        pictureBox94.ImageLocation = @"cat.png";
                        break;
                    }
                case 95:
                    {
                        pictureBox95.Visible = true;
                        pictureBox95.ImageLocation = @"cat.png";
                        break;
                    }
                case 96:
                    {
                        pictureBox96.Visible = true;
                        pictureBox96.ImageLocation = @"cat.png";
                        break;
                    }
                case 97:
                    {
                        pictureBox97.Visible = true;
                        pictureBox97.ImageLocation = @"cat.png";
                        break;
                    }
                case 98:
                    {
                        pictureBox98.Visible = true;
                        pictureBox98.ImageLocation = @"cat.png";
                        break;
                    }
                case 99:
                    {
                        pictureBox99.Visible = true;
                        pictureBox99.ImageLocation = @"cat.png";
                        break;
                    }
                case 100:
                    {
                        pictureBox100.Visible = true;
                        pictureBox100.ImageLocation = @"cat.png";
                        MessageBox.Show("Pisica a castigat!");
                        break;
                    }
            }

            switch (b.MoveB())
            {
                case 1:
                    {
                        pictureBox1.Visible = true;
                        pictureBox1.ImageLocation = @"dog.png";
                        break;
                    }
                case 2:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.ImageLocation = @"dog.png";
                        break;
                    }
                case 3:
                    {
                        pictureBox3.Visible = true;
                        pictureBox3.ImageLocation = @"dog.png";
                        break;
                    }
                case 4:
                    {
                        pictureBox4.Visible = true;
                        pictureBox4.ImageLocation = @"dog.png";
                        break;
                    }
                case 5:
                    {
                        pictureBox5.Visible = true;
                        pictureBox5.ImageLocation = @"dog.png";
                        break;
                    }
                case 6:
                    {
                        pictureBox6.Visible = true;
                        pictureBox6.ImageLocation = @"dog.png";
                        break;
                    }
                case 7:
                    {
                        pictureBox7.Visible = true;
                        pictureBox7.ImageLocation = @"dog.png";
                        break;
                    }
                case 8:
                    {
                        pictureBox8.Visible = true;
                        pictureBox8.ImageLocation = @"dog.png";
                        break;
                    }
                case 9:
                    {
                        pictureBox9.Visible = true;
                        pictureBox9.ImageLocation = @"dog.png";
                        break;
                    }
                case 10:
                    {
                        pictureBox10.Visible = true;
                        pictureBox10.ImageLocation = @"dog.png";
                        break;
                    }
                case 11:
                    {
                        pictureBox11.Visible = true;
                        pictureBox11.ImageLocation = @"dog.png";
                        break;
                    }
                case 12:
                    {
                        pictureBox12.Visible = true;
                        pictureBox12.ImageLocation = @"dog.png";
                        break;
                    }
                case 13:
                    {
                        pictureBox13.Visible = true;
                        pictureBox13.ImageLocation = @"dog.png";
                        break;
                    }
                case 14:
                    {
                        pictureBox14.Visible = true;
                        pictureBox14.ImageLocation = @"dog.png";
                        break;
                    }
                case 15:
                    {
                        pictureBox15.Visible = true;
                        pictureBox15.ImageLocation = @"dog.png";
                        break;
                    }
                case 16:
                    {
                        pictureBox16.Visible = true;
                        pictureBox16.ImageLocation = @"dog.png";
                        break;
                    }
                case 17:
                    {
                        pictureBox17.Visible = true;
                        pictureBox17.ImageLocation = @"dog.png";
                        break;
                    }
                case 18:
                    {
                        pictureBox18.Visible = true;
                        pictureBox18.ImageLocation = @"dog.png";
                        break;
                    }
                case 19:
                    {
                        pictureBox19.Visible = true;
                        pictureBox19.ImageLocation = @"dog.png";
                        break;
                    }
                case 20:
                    {
                        pictureBox20.Visible = true;
                        pictureBox20.ImageLocation = @"dog.png";
                        break;
                    }
                case 21:
                    {
                        pictureBox21.Visible = true;
                        pictureBox21.ImageLocation = @"dog.png";
                        break;
                    }
                case 22:
                    {
                        pictureBox22.Visible = true;
                        pictureBox22.ImageLocation = @"dog.png";
                        break;
                    }
                case 23:
                    {
                        pictureBox23.Visible = true;
                        pictureBox23.ImageLocation = @"dog.png";
                        break;
                    }
                case 24:
                    {
                        pictureBox24.Visible = true;
                        pictureBox24.ImageLocation = @"dog.png";
                        break;
                    }
                case 25:
                    {
                        pictureBox25.Visible = true;
                        pictureBox25.ImageLocation = @"dog.png";
                        break;
                    }
                case 26:
                    {
                        pictureBox26.Visible = true;
                        pictureBox26.ImageLocation = @"dog.png";
                        break;
                    }
                case 27:
                    {
                        pictureBox27.Visible = true;
                        pictureBox27.ImageLocation = @"dog.png";
                        break;
                    }
                case 28:
                    {
                        pictureBox28.Visible = true;
                        pictureBox28.ImageLocation = @"dog.png";
                        break;
                    }
                case 29:
                    {
                        pictureBox29.Visible = true;
                        pictureBox29.ImageLocation = @"dog.png";
                        break;
                    }
                case 30:
                    {
                        pictureBox30.Visible = true;
                        pictureBox30.ImageLocation = @"dog.png";
                        break;
                    }
                case 31:
                    {
                        pictureBox31.Visible = true;
                        pictureBox31.ImageLocation = @"dog.png";
                        break;
                    }
                case 32:
                    {
                        pictureBox32.Visible = true;
                        pictureBox32.ImageLocation = @"dog.png";
                        break;
                    }
                case 33:
                    {
                        pictureBox33.Visible = true;
                        pictureBox33.ImageLocation = @"dog.png";
                        break;
                    }
                case 34:
                    {
                        pictureBox34.Visible = true;
                        pictureBox34.ImageLocation = @"dog.png";
                        break;
                    }
                case 35:
                    {
                        pictureBox35.Visible = true;
                        pictureBox35.ImageLocation = @"dog.png";
                        break;
                    }
                case 36:
                    {
                        pictureBox36.Visible = true;
                        pictureBox36.ImageLocation = @"dog.png";
                        break;
                    }
                case 37:
                    {
                        pictureBox37.Visible = true;
                        pictureBox37.ImageLocation = @"dog.png";
                        break;
                    }
                case 38:
                    {
                        pictureBox38.Visible = true;
                        pictureBox38.ImageLocation = @"dog.png";
                        break;
                    }
                case 39:
                    {
                        pictureBox39.Visible = true;
                        pictureBox39.ImageLocation = @"dog.png";
                        break;
                    }
                case 40:
                    {
                        pictureBox40.Visible = true;
                        pictureBox40.ImageLocation = @"dog.png";
                        break;
                    }
                case 41:
                    {
                        pictureBox41.Visible = true;
                        pictureBox41.ImageLocation = @"dog.png";
                        break;
                    }
                case 42:
                    {
                        pictureBox42.Visible = true;
                        pictureBox42.ImageLocation = @"dog.png";
                        break;
                    }
                case 43:
                    {
                        pictureBox43.Visible = true;
                        pictureBox43.ImageLocation = @"dog.png";
                        break;
                    }
                case 44:
                    {
                        pictureBox44.Visible = true;
                        pictureBox44.ImageLocation = @"dog.png";
                        break;
                    }
                case 45:
                    {
                        pictureBox45.Visible = true;
                        pictureBox45.ImageLocation = @"dog.png";
                        break;
                    }
                case 46:
                    {
                        pictureBox46.Visible = true;
                        pictureBox46.ImageLocation = @"dog.png";
                        break;
                    }
                case 47:
                    {
                        pictureBox47.Visible = true;
                        pictureBox48.ImageLocation = @"dog.png";
                        break;
                    }
                case 48:
                    {
                        pictureBox48.Visible = true;
                        pictureBox48.ImageLocation = @"dog.png";
                        break;
                    }
                case 49:
                    {
                        pictureBox49.Visible = true;
                        pictureBox49.ImageLocation = @"dog.png";
                        break;
                    }
                case 50:
                    {
                        pictureBox50.Visible = true;
                        pictureBox50.ImageLocation = @"dog.png";
                        break;
                    }
                case 51:
                    {
                        pictureBox51.Visible = true;
                        pictureBox51.ImageLocation = @"dog.png";
                        break;
                    }
                case 52:
                    {
                        pictureBox52.Visible = true;
                        pictureBox52.ImageLocation = @"dog.png";
                        break;
                    }
                case 53:
                    {
                        pictureBox53.Visible = true;
                        pictureBox53.ImageLocation = @"dog.png";
                        break;
                    }
                case 54:
                    {
                        pictureBox54.Visible = true;
                        pictureBox54.ImageLocation = @"dog.png";
                        break;
                    }
                case 55:
                    {
                        pictureBox55.Visible = true;
                        pictureBox55.ImageLocation = @"dog.png";
                        break;
                    }
                case 56:
                    {
                        pictureBox56.Visible = true;
                        pictureBox56.ImageLocation = @"dog.png";
                        break;
                    }
                case 57:
                    {
                        pictureBox57.Visible = true;
                        pictureBox57.ImageLocation = @"dog.png";
                        break;
                    }
                case 58:
                    {
                        pictureBox58.Visible = true;
                        pictureBox58.ImageLocation = @"dog.png";
                        break;
                    }
                case 59:
                    {
                        pictureBox59.Visible = true;
                        pictureBox59.ImageLocation = @"dog.png";
                        break;
                    }
                case 60:
                    {
                        pictureBox60.Visible = true;
                        pictureBox60.ImageLocation = @"dog.png";
                        break;
                    }
                case 61:
                    {
                        pictureBox61.Visible = true;
                        pictureBox61.ImageLocation = @"dog.png";
                        break;
                    }
                case 62:
                    {
                        pictureBox62.Visible = true;
                        pictureBox62.ImageLocation = @"dog.png";
                        break;
                    }
                case 63:
                    {
                        pictureBox63.Visible = true;
                        pictureBox63.ImageLocation = @"dog.png";
                        break;
                    }
                case 64:
                    {
                        pictureBox64.Visible = true;
                        pictureBox64.ImageLocation = @"dog.png";
                        break;
                    }
                case 65:
                    {
                        pictureBox65.Visible = true;
                        pictureBox65.ImageLocation = @"dog.png";
                        break;
                    }
                case 66:
                    {
                        pictureBox66.Visible = true;
                        pictureBox66.ImageLocation = @"dog.png";
                        break;
                    }
                case 67:
                    {
                        pictureBox67.Visible = true;
                        pictureBox67.ImageLocation = @"dog.png";
                        break;
                    }
                case 68:
                    {
                        pictureBox68.Visible = true;
                        pictureBox68.ImageLocation = @"dog.png";
                        break;
                    }
                case 69:
                    {
                        pictureBox69.Visible = true;
                        pictureBox69.ImageLocation = @"dog.png";
                        break;
                    }
                case 70:
                    {
                        pictureBox70.Visible = true;
                        pictureBox70.ImageLocation = @"dog.png";
                        break;
                    }
                case 71:
                    {
                        pictureBox71.Visible = true;
                        pictureBox71.ImageLocation = @"dog.png";
                        break;
                    }
                case 72:
                    {
                        pictureBox72.Visible = true;
                        pictureBox72.ImageLocation = @"dog.png";
                        break;
                    }
                case 73:
                    {
                        pictureBox73.Visible = true;
                        pictureBox73.ImageLocation = @"dog.png";
                        break;
                    }
                case 74:
                    {
                        pictureBox74.Visible = true;
                        pictureBox74.ImageLocation = @"dog.png";
                        break;
                    }
                case 75:
                    {
                        pictureBox75.Visible = true;
                        pictureBox75.ImageLocation = @"dog.png";
                        break;
                    }
                case 76:
                    {
                        pictureBox76.Visible = true;
                        pictureBox76.ImageLocation = @"dog.png";
                        break;
                    }
                case 77:
                    {
                        pictureBox77.Visible = true;
                        pictureBox77.ImageLocation = @"dog.png";
                        break;
                    }
                case 78:
                    {
                        pictureBox78.Visible = true;
                        pictureBox78.ImageLocation = @"dog.png";
                        break;
                    }
                case 79:
                    {
                        pictureBox79.Visible = true;
                        pictureBox79.ImageLocation = @"dog.png";
                        break;
                    }
                case 80:
                    {
                        pictureBox80.Visible = true;
                        pictureBox80.ImageLocation = @"dog.png";
                        break;
                    }
                case 81:
                    {
                        pictureBox81.Visible = true;
                        pictureBox81.ImageLocation = @"dogdog.png";
                        break;
                    }
                case 82:
                    {
                        pictureBox82.Visible = true;
                        pictureBox82.ImageLocation = @"dog.png";
                        break;
                    }
                case 83:
                    {
                        pictureBox83.Visible = true;
                        pictureBox83.ImageLocation = @"dog.png";
                        break;
                    }
                case 84:
                    {
                        pictureBox84.Visible = true;
                        pictureBox84.ImageLocation = @"dog.png";
                        break;
                    }
                case 85:
                    {
                        pictureBox85.Visible = true;
                        pictureBox85.ImageLocation = @"dog.png";
                        break;
                    }
                case 86:
                    {
                        pictureBox86.Visible = true;
                        pictureBox86.ImageLocation = @"dog.png";
                        break;
                    }
                case 87:
                    {
                        pictureBox87.Visible = true;
                        pictureBox87.ImageLocation = @"dog.png";
                        break;
                    }
                case 88:
                    {
                        pictureBox88.Visible = true;
                        pictureBox88.ImageLocation = @"dog.png";
                        break;
                    }
                case 89:
                    {
                        pictureBox89.Visible = true;
                        pictureBox89.ImageLocation = @"dog.png";
                        break;
                    }
                case 90:
                    {
                        pictureBox90.Visible = true;
                        pictureBox90.ImageLocation = @"dog.png";
                        break;
                    }
                case 91:
                    {
                        pictureBox91.Visible = true;
                        pictureBox91.ImageLocation = @"dog.png";
                        break;
                    }
                case 92:
                    {
                        pictureBox92.Visible = true;
                        pictureBox92.ImageLocation = @"dog.png";
                        break;
                    }
                case 93:
                    {
                        pictureBox93.Visible = true;
                        pictureBox93.ImageLocation = @"dog.png";
                        break;
                    }
                case 94:
                    {
                        pictureBox94.Visible = true;
                        pictureBox94.ImageLocation = @"dog.png";
                        break;
                    }
                case 95:
                    {
                        pictureBox95.Visible = true;
                        pictureBox95.ImageLocation = @"dog.png";
                        break;
                    }
                case 96:
                    {
                        pictureBox96.Visible = true;
                        pictureBox96.ImageLocation = @"dog.png";
                        break;
                    }
                case 97:
                    {
                        pictureBox97.Visible = true;
                        pictureBox97.ImageLocation = @"dog.png";
                        break;
                    }
                case 98:
                    {
                        pictureBox98.Visible = true;
                        pictureBox98.ImageLocation = @"dog.png";
                        break;
                    }
                case 99:
                    {
                        pictureBox99.Visible = true;
                        pictureBox99.ImageLocation = @"dog.png";
                        break;
                    }
                case 100:
                    {
                        pictureBox100.Visible = true;
                        pictureBox100.ImageLocation = @"dog.png";
                        MessageBox.Show("Catelul a castigat!");
                        break;
                    }
            }
            b.Switch();

        }
    }
}
