﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    internal class Player: Zar
    {
        protected int turn;

        public Player()
        {
            turn = 0;
        }

        public int Switch()
        {
            if (number != 6)
            {
                if (turn == 0)
                    turn = 1;
                else if (turn == 1)
                    turn = 0;
            }
            if (turn != 0 && turn != 1)
                MessageBox.Show("Fatal Error");
            return turn;
        }

        public int getTurn()
        {
            return turn;
        }
    }
}
